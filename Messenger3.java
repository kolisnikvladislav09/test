import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.Group;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Messenger3 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
       Group group = new Group();

        primaryStage.setTitle("Messenger");

        Parent content = FXMLLoader.load(getClass().getResource("myForm.fxml"));
        BorderPane root = new BorderPane();
        root.setCenter(content);
        group.getChildren().add(root);

       primaryStage.setScene(new Scene(group, 600,500));
       primaryStage.show();

    }
    public class obrobnykPodij implements ActionListener{
        public void actionPerformed (ActionEvent podija) {
           Cont_auvt v = new Cont_auvt();
           v.setTitle("Messenger");
           v.setSize(800,600);
           v.setVisible(true);
           v.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }
}
